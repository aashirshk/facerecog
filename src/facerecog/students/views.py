from django.http import HttpResponse
from django.shortcuts import render, redirect
from .models import Students, Attendance

import cv2
import os
import shutil
import time

# Create your views here.
Id = 0
Name = None

def student_save(request, *args, **kwargs):
    global Id
    global Name
    Id = request.POST['std_id']
    Name=request.POST['std_fullname']
    print('Id: {}'.format(Id))
    Students.objects.create(Id=Id,
                            Name=Name,
                            Faculty=request.POST['std_faculty'],
                            Semester=request.POST['std_sem'],
                            Phone_Number=request.POST['std_number'],
                            Address=request.POST['address'],
                            Email_Address=request.POST['std_email'])
    os.chdir('/home/aashir/Documents/7th Sem/bcp/StudentFaces')
    try:
        os.mkdir(Id)
    except FileExistsError:
        os.rename(Id, Id)
    return render(request, 'camera-open.html')


def student_edit(request, id, *args, **kwargs):
    student = Students.objects.get(Id=id)
    if request.method == "POST":
        Name = request.POST['std_fullname']
        Faculty = request.POST['std_faculty']
        Semester = request.POST['std_sem']
        Phone_Number = request.POST['std_number']
        Address = request.POST['address']
        Email_Address = request.POST['std_email']

        student.Name = Name
        student.Faculty = Faculty
        student.Semester = Semester
        student.Phone_Number = Phone_Number
        student.Address = Address
        student.Email_Address = Email_Address
        student.save()

        return redirect('manage_view')

    else:
        myContext = {
            "students": student,
            "action": "edit"
        }

        return render(request, 'register-student.html', myContext)


def student_delete(request, id, *args, **kwargs):
    student = Students.objects.get(Id=id)
    os.chdir('/home/aashir/Documents/7th Sem/bcp/StudentFaces')
    if os.path.exists(str(student.Id)) and not os.listdir(str(student.Id)):
        os.rmdir(str(student.Id))
        print('I m empty')
    else:
        shutil.rmtree(str(student.Id))
        print('I m not empty')
    student.delete()
    return redirect('manage_view')


def camera(request, *args, **kwargs):
    global Id
    global Name
    print("Id {}".format(Id))
    print("Name {}".format(Name))

    def draw_border(img, pt1, pt2, color, thickness, r, d):
        x1, y1 = pt1
        x2, y2 = pt2

        # Top left
        cv2.line(img, (x1 + r, y1), (x1 + r + d, y1), color, thickness)
        cv2.line(img, (x1, y1 + r), (x1, y1 + r + d), color, thickness)
        cv2.ellipse(img, (x1 + r, y1 + r), (r, r), 180, 0, 90, color, thickness)

        # Top right
        cv2.line(img, (x2 - r, y1), (x2 - r - d, y1), color, thickness)
        cv2.line(img, (x2, y1 + r), (x2, y1 + r + d), color, thickness)
        cv2.ellipse(img, (x2 - r, y1 + r), (r, r), 270, 0, 90, color, thickness)

        # Bottom left
        cv2.line(img, (x1 + r, y2), (x1 + r + d, y2), color, thickness)
        cv2.line(img, (x1, y2 - r), (x1, y2 - r - d), color, thickness)
        cv2.ellipse(img, (x1 + r, y2 - r), (r, r), 90, 0, 90, color, thickness)

        # Bottom right
        cv2.line(img, (x2 - r, y2), (x2 - r - d, y2), color, thickness)
        cv2.line(img, (x2, y2 - r), (x2, y2 - r - d), color, thickness)
        cv2.ellipse(img, (x2 - r, y2 - r), (r, r), 0, 0, 90, color, thickness)

    face_recog = cv2.CascadeClassifier(
        '/home/aashir/Dev13/attendanceSystem/lib/python3.6/site-packages/cv2/data/haarcascade_frontalface_default.xml')

    print("Face Recog {}".format(face_recog))
    cap = cv2.VideoCapture(2)
    count = 0
    while (True):
        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = face_recog.detectMultiScale(gray, 1.3, 5)
        for (x, y, w, h) in faces:
            count += 1
            # print(x, y, w, h)
            roi_gray = gray[y:y + h, x:x + w]
            roi_color = frame[y:y + h, x:x + w]
            # cv2.imwrite("/home/aashir/Documents/7th Sem/bcp/{}/user" + str(count) + ".jpg", roi_color)
            print("TYPE OF ID: {}".format(type(Id)))
            cv2.imwrite("/home/aashir/Documents/7th Sem/bcp/StudentFaces/{}/{}".format(int(Id), Name) + str(count) + ".jpg", roi_color)
            cv2.putText(frame, str(count), (50, 50), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 0), 2)
            draw_border(frame, (x, y), (x + w, y + h), (255, 0, 0), 2, 10, 20)

        cv2.imshow('Face', frame)

        if cv2.waitKey(100) == 13 or count == 100:
            break

    cap.release()
    cv2.destroyAllWindows()

    return render(request, 'train-image.html')
