from django.db import models

# Create your models here.


class Students(models.Model):
    Id = models.IntegerField(primary_key=True)
    Name = models.CharField(max_length=30)
    Faculty = models.CharField(max_length=15)
    Semester = models.CharField(max_length=15)
    Phone_Number = models.IntegerField()
    Address = models.CharField(max_length=20)
    Email_Address = models.EmailField()


class Attendance(models.Model):
    id = models.AutoField(primary_key=True)
    Student_Id = models.IntegerField()
    Name = models.CharField(max_length=30)
    status = models.IntegerField()
    date = models.CharField(max_length=20)