# Generated by Django 2.2 on 2020-03-18 06:31

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Attendance',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('Student_Id', models.IntegerField()),
                ('Name', models.CharField(max_length=30)),
                ('status', models.IntegerField()),
                ('date', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Students',
            fields=[
                ('Id', models.IntegerField(primary_key=True, serialize=False)),
                ('Name', models.CharField(max_length=30)),
                ('Faculty', models.CharField(max_length=15)),
                ('Semester', models.CharField(max_length=15)),
                ('Phone_Number', models.IntegerField()),
                ('Address', models.CharField(max_length=20)),
                ('Email_Address', models.EmailField(max_length=254)),
            ],
        ),
    ]
