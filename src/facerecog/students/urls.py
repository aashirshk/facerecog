from django.urls import path
from .views import student_save, camera, student_edit
from pages.facetrain import train

urlpatterns = [
    path('save/', student_save, name="save"),
    path('edit/<int:id>', student_edit, name="edit"),
    path('camera/', camera, name="camera"),
    path('train/', train, name="train")
]