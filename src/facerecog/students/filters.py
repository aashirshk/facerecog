import django_filters
from django.db import models
from django import forms

from .models import Students


class StudentFilter(django_filters.FilterSet):
    Name = forms.CharField(max_length=20,
                            widget=forms.TextInput(
                                attrs={
                                    'placeholder': 'Enter the title',
                                }
                            )

                            )
    class Meta:
        model = Students
        fields = '__all__'
        filter_overrides = {
            models.CharField: {
                'filter_class': django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'icontains',

                },

            }
        }
        exclude = ['Id', 'Faculty', 'Phone_Number', 'Email_Address']
