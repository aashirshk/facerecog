from django.shortcuts import render, redirect
from admin import views
from students.models import Students, Attendance
import django_eel as eel
from students.filters import StudentFilter
import cv2
import pickle
import datetime
import time

Id = 0
Name = ""
filterdata = ""
status = []
status1 = {}
stuStatus = object
status2 = 0



# Create your views here.
def admin_view(request, *args, **kwargs):
    views.isLoggedIn = False
    return render(request, 'login.html')


def dashboard(request, *args, **kwargs):
    return render(request, 'dashboard.html')


def register_view(request, *args, **kwargs):
    return render(request, 'register-student.html')

attdDate = ""

def attendence_view(request, *args, **kwargs):
    global stuStatus
    global Id
    global Name
    global attdDate
    # sysDate = time.strftime("%d-%B-%Y")
    allAttendance = Attendance.objects.all()
    # if not allAttendance.exists():
    #     Attendance.objects.create(Id=)



    students = Students.objects.all()

    for std in students:
        studentId = std.Id
        try:
            stuStatus =Attendance.objects.get(Student_Id=studentId)
            if stuStatus.status == 0:
                print("I am zero")
                status.append(0)
                status1[studentId] = 0
            else:
                print("I am one")
                status1[studentId] = 1
                status.append(1)

            if stuStatus.date:
                attdDate = stuStatus.date
            else:
                attdDate = time.strftime("%d-%B-%Y")
                # status = stuStatus.status
                # print("status : {}".format(status))
        except Exception:
            print("I am exception")
            attdDate = time.strftime("%d-%B-%Y")
            status1[studentId] = 0
            status.append(0)
        print("status1 : {}".format(status1))

    context = {
        'std': students,
        'status': status,
        'status1': status1,
        'flag': 0,
        'date': attdDate
    }
    print(context['status1'])
    # print(stuStatus['date'])


    return render(request, 'attendence-sheet.html', context)


def manage_view(request, *args, **kwargs):
    obj = Students.objects.all()
    studentFilter = StudentFilter(request.GET, queryset=obj)
    obj = studentFilter.qs
    my_context = {
        'student': obj,
        'student_filter': studentFilter

    }
    return render(request, 'student-table.html', my_context)


def home_view(request, *args, **kwargs):
    tday = time.strftime("%A")
    tmonth = time.strftime("%B")
    tdate = time.strftime("%Y/%m/%d")
    now = datetime.datetime.now()
    print(now)
    print(datetime.time())
    timezone = time.ctime(time.time())
    print(timezone)
    # day, month, date, currenttime, year = timezone.split(" ")
    # print(timezone)
    # hour, minute, sec = currenttime.split(":")
    # print(currenttime)
    # print(hour)
    # if int(hour) >= 5 or int(hour) < 12:
    #     greeting = "Good Morning"
    # elif int(hour) >= 12 or int(hour) < 17:
    #     greeting = "Good Afternoon"
    # elif int(hour) >= 18 or int(hour) < 22:
    #     greeting = "Good Evening"
    # else:
    #     greeting = "Good Night"

    # print(greeting)
    context = {
        'day': tday,
        'month': tmonth,
        'date': tdate,
        # 'greeting': greeting
    }
    return render(request, 'index.html', context)


def recog_camera(request, *args, **kwargs):
    global status2
    global Id
    global Name
    sysDate = time.strftime("%d-%B-%Y")
    face_recog = cv2.CascadeClassifier(
        '/home/aashir/Dev13/attendanceSystem/lib/python3.6/site-packages/cv2/data/haarcascade_frontalface_default.xml')
    recognizer = cv2.face.LBPHFaceRecognizer_create()
    recognizer.read("/home/aashir/Dev13/attendanceSystem/src/facerecog/pages/trainer.yml")
    labels = {"person_name": 1}
    with open("/home/aashir/Dev13/attendanceSystem/src/facerecog/pages/labels.pickle", "rb") as f:
        og_labels = pickle.load(f)
        labels = {v: k for k, v in og_labels.items()}

    cap = cv2.VideoCapture(2)
    count = 0
    while (True):
        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = face_recog.detectMultiScale(gray, 1.3, 5)
        for (x, y, w, h) in faces:
            count += 1
            # print(x, y, w, h)
            roi_gray = gray[y:y + h, x:x + w]
            roi_color = frame[y:y + h, x:x + w]
            id_, conf = recognizer.predict(roi_gray)
            if conf >= 30 and conf <= 85:
                print(id_)
                print(labels[id_])
                Id = id_
                Name = labels[id_]
                font = cv2.FONT_HERSHEY_SIMPLEX
                name = labels[id_]
                color = (255, 255, 255)
                stroke = 2
                cv2.putText(frame, name, (x, y), font, 1, color, stroke, cv2.LINE_AA)



            # cv2.imwrite("/home/aashir/Documents/7th Sem/bcp/{}/user" + str(count) + ".jpg", roi_color)
            # cv2.imwrite("/home/aashir/Documents/7th Sem/bcp/StudentFaces/{}/user".format(int(Id)) + str(count) + ".jpg", roi_color)
            # cv2.putText(frame, str(count), (50, 50), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 0), 2)
            draw_border(frame, (x, y), (x + w, y + h), (255, 0, 0), 2, 10, 20)


        cv2.imshow('Face', frame)

        if cv2.waitKey(100) == 13 or count == 100:
            break

    if Id > 0 and Name != "":
        verifiedId = Id
        # Attendance.objects.create(Student_Id=verifiedId, Name=Name, status=1, date=sysDate)
        attendance = Attendance.objects.all()
        if not attendance.exists():
            status2 = 1
            Attendance.objects.create(Student_Id=verifiedId, Name=Name, status=status2, date=sysDate)
        else:
            stdAttd = Attendance.objects.filter(Student_Id=verifiedId)
            if not stdAttd.exists():
                Attendance.objects.create(Student_Id=verifiedId, Name=Name, status=1, date=sysDate)
            elif stdAttd.exists() and stdAttd.date == sysDate and stdAttd == 0:
                spAttendance = Attendance.objects.get(Student_Id=verifiedId)
                spAttendance.status = 1
                spAttendance.save()
            else:
                print("Attendance taken")
        for indStd in Students.object.all():
            if Id == indStd.Id:
                continue
            else:
                Attendance.objects.create(Student_Id=verifiedId, Name=Name, status=1, date=sysDate)




    cap.release()
    cv2.destroyAllWindows()

    return redirect('home_view')


def draw_border(img, pt1, pt2, color, thickness, r, d):
    x1, y1 = pt1
    x2, y2 = pt2

    # Top left
    cv2.line(img, (x1 + r, y1), (x1 + r + d, y1), color, thickness)
    cv2.line(img, (x1, y1 + r), (x1, y1 + r + d), color, thickness)
    cv2.ellipse(img, (x1 + r, y1 + r), (r, r), 180, 0, 90, color, thickness)

    # Top right
    cv2.line(img, (x2 - r, y1), (x2 - r - d, y1), color, thickness)
    cv2.line(img, (x2, y1 + r), (x2, y1 + r + d), color, thickness)
    cv2.ellipse(img, (x2 - r, y1 + r), (r, r), 270, 0, 90, color, thickness)

    # Bottom left
    cv2.line(img, (x1 + r, y2), (x1 + r + d, y2), color, thickness)
    cv2.line(img, (x1, y2 - r), (x1, y2 - r - d), color, thickness)
    cv2.ellipse(img, (x1 + r, y2 - r), (r, r), 90, 0, 90, color, thickness)

    # Bottom right
    cv2.line(img, (x2 - r, y2), (x2 - r - d, y2), color, thickness)
    cv2.line(img, (x2, y2 - r), (x2, y2 - r - d), color, thickness)
    cv2.ellipse(img, (x2 - r, y2 - r), (r, r), 0, 0, 90, color, thickness)
