"""facerecog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from admin.views import admin_login
from pages.views import register_view, manage_view, attendence_view, home_view, recog_camera
from students.views import student_delete


urlpatterns = [
    path('admin/', include('admin.urls')),
    path('dashboard/', admin_login, name="dashboard"),
    path('dashboard/register-user/', register_view, name="register_view"),
    path('dashboard/attendence-sheet/', attendence_view, name="attendence_view"),
    path('dashboard/manage-user/', manage_view, name="manage_view"),
    path('dashboard/manage-user/delete/<int:id>', student_delete, name="delete"),
    path('dashboard/register-user/student/', include('students.urls'), name="student"),
    path('', home_view, name="home_view"),
    path('camera', recog_camera, name="recog_camera"),

]
