from django.db import models

# Create your models here.
class Attendance(models.Model):
    Id = models.IntegerField(primary_key=True)
    Name = models.CharField(max_length=30)
    status = models.IntegerField()
    date = models.CharField(max_length=20)

