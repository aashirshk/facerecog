from django.urls import path
from pages.views import admin_view
from .views import admin_login

urlpatterns = [
    path('', admin_view, name="admin"),
]