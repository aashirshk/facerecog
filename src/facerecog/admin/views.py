from django.shortcuts import render, redirect
from .models import Admin
from students.models import Students
import eel

# Create your views here.

isLoggedIn = False
error = True
studentCount = {}
def admin_login(request, *args, **kwargs):
    global error
    global studentCount
    global isLoggedIn

    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        if username and password:
            try:
                admin = Admin.objects.get(username=username, password=password)
                if admin:
                    error = False

                isLoggedIn = True

                count = Students.objects.all().count()

                studentCount = {
                    'total_std': count,
                    'hasError': error
                }

            except Exception:
                error = True
                return redirect('admin')

            print(studentCount['hasError'])



            return render(request, 'dashboard.html', studentCount)


    if isLoggedIn:
        print('Logged in')
        return render(request, 'dashboard.html', studentCount)
    else:
        return redirect('admin')




